import {DAO} from "./interface.dao";
import {IMission} from "../definitions";

export interface IMissionDAO extends DAO<IMission> {}
