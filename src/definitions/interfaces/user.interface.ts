export interface IUser {
  id: string;
  email: string;
  firstName: string;
  lastName: string;
  premium_until?: string;
  profile: IUserProfile;
}

export interface IUserProfile {
  name: string;
  status: number;
  skills: Array<string>;
  siret: string;
  certified: number;
}
