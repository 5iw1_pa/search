import * as dotenv from 'dotenv';
dotenv.config();
import * as http from 'http';
import { IMissionService, ISearchService, IUserService } from './definitions';
import {ContainerManager, SHARED_TYPES} from './inversify';
import {appModule, serviceModule, elascticsearchModule, SearchService, MissionService, UserService} from './platforms';

const containerManager = ContainerManager.getInstance();

async function main(): Promise<http.Server[]> {
  await containerManager.load([
    serviceModule,
  ], [
    appModule,
    elascticsearchModule,
  ]);
  // We need to init search service to create channel for rabbitMq
  containerManager.container.get<ISearchService>(SearchService);
  containerManager.container.get<IMissionService>(MissionService);
  containerManager.container.get<IUserService>(UserService);
  return [
    containerManager.container.getNamed(SHARED_TYPES.Server, 'main')
  ];
}

main().catch(console.error);
