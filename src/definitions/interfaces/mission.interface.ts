export interface IMission {
  id: string;
  title: string;
  description: string;
  status: number;
  skills: Array<string>;
  profile: IMissionProfile;
  startDate: string;
  endDate: string;
}

export interface IMissionProfile {
  premium_until: string;
  name: string;
}

export interface IDecodedMission {
  id: string;
  title: string;
  description: string;
  status: number;
  skills: Array<string>;
  idAuthor: IMissionAuthor;
  startDate: string;
  endDate: string;
}

export interface IMissionAuthor {
  profile: IMissionProfile;
}
