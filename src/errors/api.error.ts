import * as fs from 'fs';

export class ApiError extends Error {
  httpCode: number;
  path: string;
  method: string;
  date: Date;
  msg: string;

  public constructor(httpCode: number, message: string, path?: string, method?: string) {
    super(message);
    // Set the prototype explicitly.
    Object.setPrototypeOf(this, ApiError.prototype);
    this.httpCode = httpCode;
    this.msg = message;
    this.path = path || '';
    this.method = method || '';
    this.date = new Date();

  }

  public static isApiError(error: any): boolean {
    return typeof error.msg === 'string' && Number.isInteger(error.httpCode);
  }

  public static async saveLog(apiError: ApiError): Promise<void> {
    const log = `###########\nAn Error occured /!\\ \n=> date: ${apiError.date}\n=> http code: ${apiError.httpCode}\n=> message: ${apiError.message}\n###########\n`;
    fs.appendFileSync('errors.log', log);
    return;
  }
}
