import {interfaces} from 'inversify';
import {ConnectUtils} from './utils';
import {AsyncUnloaderContainerModule, SHARED_TYPES} from '../../inversify';
import {TYPES} from './inversify';
import {IMissionDAO, ISearchDAO, IUserDAO} from '../../dao';
import {SearchDAO, MissionDAO, UserDAO} from './dao';
import {Client} from '@elastic/elasticsearch';

export const elascticsearchModule = new AsyncUnloaderContainerModule(async (bind: interfaces.Bind, unbind: interfaces.Unbind, isBound: interfaces.IsBound): Promise<void> => {
  if (!isBound(TYPES.Connection)) {
    const connection = await ConnectUtils.open();
    bind<Client>(TYPES.Connection).toConstantValue(connection);
    bind<Client>(Client).toConstantValue(connection);
  }

  //DAOs
  if (!isBound(SHARED_TYPES.DAO.Search)) {
    bind<ISearchDAO>(SHARED_TYPES.DAO.Search).to(SearchDAO).inSingletonScope();
    bind<IMissionDAO>(SHARED_TYPES.DAO.Mission).to(MissionDAO).inSingletonScope();
    bind<IUserDAO>(SHARED_TYPES.DAO.User).to(UserDAO).inSingletonScope();
  }
}, async (container: interfaces.Container) => {
  if (container.isBound(Client)) {
    const elasticsearch = container.get<Client>(Client);
    await elasticsearch.close();
  }
});
