export interface IAmqpService {
  connect(): Promise<void>;
  registerQueue(queueName: string, exchange:string, callBack: any): Promise<void>
}
