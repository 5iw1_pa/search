import { inject, injectable } from "inversify";
import { IAmqpService, ISearch, ISearchService } from "../../../definitions";
import { ApiError } from "../../../errors";
import { SHARED_TYPES } from "../../../inversify";
import { ISearchDAO } from "../../../dao";

@injectable()
export class SearchService implements ISearchService {
  private readonly amqpService: IAmqpService;
  private readonly searchDAO: ISearchDAO;

  public constructor(@inject(SHARED_TYPES.Services.Amqp) amqpService: IAmqpService,
                     @inject(SHARED_TYPES.DAO.Search) searchDAO: ISearchDAO) {
    this.amqpService = amqpService;
    this.searchDAO = searchDAO;
    this.init();
  }

  public async init(): Promise<void> {
    this.amqpService.registerQueue("search:create", "darkmalt.exchange:search.create", this.create.bind(this));
    this.amqpService.registerQueue("search:update", "darkmalt.exchange:search.update", this.updateOne.bind(this));
    this.amqpService.registerQueue("search:delete", "darkmalt.exchange:search.delete", this.deleteOne.bind(this));
  }


  public async create(data: any): Promise<void> {
    const search = this.searchDAO.parseRabbitMqMessage<ISearch>(data);
    await this.searchDAO.create(search);
  }

  public async updateOne(data: string): Promise<void> {
    const search = this.searchDAO.parseRabbitMqMessage<ISearch>(data);
    if (!search.id) {
      const err = new ApiError(400, "Update/Search id is required");
      ApiError.saveLog(err);
      throw err;
    };
    await this.searchDAO.update(search.id, search);
  }

  public async deleteOne(data: string): Promise<void> {
    const search = this.searchDAO.parseRabbitMqMessage<ISearch>(data);
    if (!search.id) {
      const err = new ApiError(400, "Delete/Search id is required");
      ApiError.saveLog(err);
      throw err;
    };
    await this.searchDAO.delete(search.id);
  }
}
