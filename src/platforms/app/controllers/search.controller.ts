import { Get, JsonController, Param, QueryParams } from "routing-controllers";
import { OpenAPI } from "routing-controllers-openapi";
import { inject, injectable } from "inversify";
import { SHARED_TYPES } from "../../../inversify";
import { ISearchDAO } from "../../../dao";
import { ISearch } from "../../../definitions";
import { SearchParamsDTO } from "../../../dto";

@injectable()
@OpenAPI({ summary: "Search Routes" })
@JsonController("/search")
export class SearchController {
  @inject(SHARED_TYPES.DAO.Search)
  private readonly searchDAO!: ISearchDAO;

  constructor() {}

  @Get("/:id")
  @OpenAPI({ summary: "Return One Search by id" })
  public async getOne(@Param("id") id: string): Promise<ISearch | null> {
    return this.searchDAO.findById(id);
  }

  @Get("/")
  @OpenAPI({ summary: "Get All Search" })
  public async getAll(@QueryParams() params: SearchParamsDTO): Promise<ISearch[]> {
    return this.searchDAO.all<ISearch>(params, []);
  }
}
