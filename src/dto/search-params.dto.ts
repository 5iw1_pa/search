import { IsOptional, IsString } from "class-validator";
import { Expose } from "class-transformer";

export class SearchParamsDTO {
  @IsString()
  @IsOptional()
  @Expose()
  s?: string;
}
