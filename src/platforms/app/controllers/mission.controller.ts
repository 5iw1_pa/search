import { inject, injectable } from "inversify";
import { Get, JsonController, Param, QueryParams } from "routing-controllers";
import { OpenAPI } from "routing-controllers-openapi";
import { IMissionDAO } from "../../../dao";
import { SHARED_TYPES } from "../../../inversify";
import { SearchParamsDTO } from '../../../dto';
import { IMission } from "../../../definitions";

@injectable()
@OpenAPI({ summary: "Mission Routes" })
@JsonController("/mission")
export class MissionController {
  @inject(SHARED_TYPES.DAO.Mission)
  private readonly missionDAO!: IMissionDAO;

  constructor() {};

  @Get("/:id")
  @OpenAPI({ summary: "Return One Mission by id" })
  public async getOne(@Param("id") id: string): Promise<any> {
    return this.missionDAO.findById(id);
  }

  @Get("/")
  @OpenAPI({ summary: "Get All Missions" })
  public async getAll(@QueryParams() params: SearchParamsDTO): Promise<IMission[]> {
    const missions = await this.missionDAO.all<IMission>(params, ["title", "description", "skills", "profile.name"]);
    return missions.filter((mission: IMission) => mission.status === 0);
  }
}
