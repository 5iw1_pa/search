import {Client} from "@elastic/elasticsearch";

export class ConnectUtils {
  static async open(): Promise<Client> {
    let retry = 0;
    let lastError: Error | undefined = undefined;
    do {
      try {
        return new Client({
          node: process.env.ELASTICSEARCH_URI,
          headers: {
            'Content-Type': 'application/json',
          }
        });
      } catch (err: any) {
        lastError = err;
        console.error(err);
        await new Promise(((resolve) => setTimeout(resolve, 1000))); // wait 1 second
        retry++;
      }
    } while (retry < 5);
    throw lastError;
  }
}
