import {interfaces} from 'inversify';
import {SHARED_TYPES, UnloaderContainerModule} from '../../inversify';
import Axios, {AxiosInstance} from 'axios';
import {TYPES} from './inversify';
import {ISearchService, IAmqpService, IMissionService, IUserService} from "../../definitions";
import {SearchService, AmqpService, MissionService, UserService} from "./services";

export const serviceModule = new UnloaderContainerModule(async (bind: interfaces.Bind) => {
  bind<AxiosInstance>(TYPES.PrivateRequest).toConstantValue(createPrivateAxiosRequest());
  bindAmqp(bind);
  bindSearch(bind);
  bindMission(bind);
  bindUser(bind);
  bind<ISearchService>(SHARED_TYPES.Services.Search).to(SearchService).inSingletonScope();
  bind<IMissionService>(SHARED_TYPES.Services.Mission).to(MissionService).inSingletonScope();
});

function bindAmqp(bind: interfaces.Bind) {
  bind(AmqpService).to(AmqpService).inSingletonScope();
  bind<IAmqpService>(SHARED_TYPES.Services.Amqp).toDynamicValue( context => {
    return context.container.get(AmqpService);
  });
}

function bindSearch(bind: interfaces.Bind) {
  bind(SearchService).to(SearchService).inSingletonScope();
  bind<ISearchService>(SHARED_TYPES.Services.Search).toDynamicValue( context => {
    return context.container.get(SearchService);
  });
}

function bindMission(bind: interfaces.Bind) {
  bind(MissionService).to(MissionService).inSingletonScope();
  bind<IMissionService>(SHARED_TYPES.Services.Mission).toDynamicValue( context => {
    return context.container.get(MissionService);
  });
}

function bindUser(bind: interfaces.Bind) {
  bind(UserService).to(UserService).inSingletonScope();
  bind<IUserService>(SHARED_TYPES.Services.User).toDynamicValue( context => {
    return context.container.get(UserService);
  });
}

function createPrivateAxiosRequest(): AxiosInstance {
  return Axios.create({
    proxy: false
  });
}
