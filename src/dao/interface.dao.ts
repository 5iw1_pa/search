import { SearchParamsDTO } from "../dto";

export interface DAO<C> {
  all<T>(
    params: SearchParamsDTO,
    fields: Array<string>,
    limit?: number,
    offset?: number
  ): Promise<C[]>;
  findById(id: string): Promise<C | null>;
  delete(id: string): Promise<boolean>;
  create(data: C): Promise<string>; // Return id of created object
  update(id: string, data: C): Promise<boolean>;
  parseRabbitMqMessage<C>(data: any): C;
}
