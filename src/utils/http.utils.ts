import * as http from 'http';

export class HttpUtils {
    static close(server: http.Server): Promise<void> {
        return new Promise<void>((resolve, reject) => {
            server.close((err?: Error) => {
                if (err) {
                    reject(err);
                    return;
                }
                resolve();
            });
        });
    }
}