import {DAO} from "./interface.dao";
import {ISearch} from "../definitions";

export interface ISearchDAO extends DAO<ISearch> {}
