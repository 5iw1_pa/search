import { IUser } from "../interfaces";

export interface IUserService {
  init(): Promise<void>;
  create(data: IUser): Promise<void>;
  updateOne(id: string, data: IUser): Promise<void>;
  deleteOne(id: string): Promise<void>;
}
