import { inject, injectable } from "inversify";
import { IAmqpService, IUser, IUserService } from "../../../definitions";
import { ApiError } from "../../../errors";
import { SHARED_TYPES } from "../../../inversify";
import { IUserDAO } from "../../../dao";

@injectable()
export class UserService implements IUserService {
  private readonly amqpService: IAmqpService;
  private readonly userDAO: IUserDAO;

  public constructor(@inject(SHARED_TYPES.Services.Amqp) amqpService: IAmqpService,
                     @inject(SHARED_TYPES.DAO.User) userDAO: IUserDAO) {
    this.amqpService = amqpService;
    this.userDAO = userDAO;
    this.init();
  }

  public async init(): Promise<void> {
    this.amqpService.registerQueue("user:create", "darkmalt.exchange:user.create", this.create.bind(this));
    this.amqpService.registerQueue("user:update", "darkmalt.exchange:user.update", this.updateOne.bind(this));
    this.amqpService.registerQueue("user:delete", "darkmalt.exchange:user.delete", this.deleteOne.bind(this));
  }


  public async create(data: any): Promise<void> {
    const user = this.userDAO.parseRabbitMqMessage<IUser>(data);
    await this.userDAO.create(user);
  }

  public async updateOne(data: string): Promise<void> {
    const user = this.userDAO.parseRabbitMqMessage<IUser>(data);
    if (!user.id) {
      const err = new ApiError(400, "Update/User id is required")
      ApiError.saveLog(err);
      throw err;
    };
    await this.userDAO.update(user.id, user);
  }

  public async deleteOne(data: string): Promise<void> {
    const user = this.userDAO.parseRabbitMqMessage<IUser>(data);
    if (!user.id) {
      const err = new ApiError(400, "Delete/User id is required");
      ApiError.saveLog(err);
      throw err;
    };
    await this.userDAO.delete(user.id);
  }
}
