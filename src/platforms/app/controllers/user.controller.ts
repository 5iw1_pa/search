import { Get, JsonController, Param, QueryParams } from "routing-controllers";
import { OpenAPI } from "routing-controllers-openapi";
import { inject, injectable } from "inversify";
import { SHARED_TYPES } from "../../../inversify";
import { IUserDAO } from "../../../dao";
import { IUser } from "../../../definitions";
import { SearchParamsDTO } from "../../../dto";

@injectable()
@OpenAPI({ summary: "User Routes" })
@JsonController("/user")
export class UserController {
  @inject(SHARED_TYPES.DAO.User)
  private readonly userDAO!: IUserDAO;

  constructor() {}

  @Get("/:id")
  @OpenAPI({ summary: "Return One User by id" })
  public async getOne(@Param("id") id: string): Promise<IUser | null> {
    return this.userDAO.findById(id);
  }

  @Get("/")
  @OpenAPI({ summary: "Get All User" })
  public async getAll(@QueryParams() params: SearchParamsDTO): Promise<IUser[]> {
    const users = await this.userDAO.all<IUser>(params, ["profile.name", "profile.siret", "email", "firstname", "lastname", "profile.skills"]);
    return users;
  }
}
