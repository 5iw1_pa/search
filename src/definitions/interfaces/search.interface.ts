export interface ISearch {
  id?: string,
  name: string,
  value: string,
}
