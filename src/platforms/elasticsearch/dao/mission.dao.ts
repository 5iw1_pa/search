import {AbstractElasticSearchDAO} from './abstract-es.dao';
import {IMissionDAO} from '../../../dao';
import {inject, injectable} from 'inversify';
import {TYPES} from '../inversify';
import { Client } from '@elastic/elasticsearch';
import { IMission } from '../../../definitions';

@injectable()
export class MissionDAO extends AbstractElasticSearchDAO<IMission> implements IMissionDAO {
  get index(): string {
    return 'mission';
  }

  public constructor(@inject(TYPES.Connection) elasticsearch: Client) {
    super(elasticsearch);
  }

  get model(): Client {
    return this.elasticsearch;
  }
}
