import { inject, injectable } from "inversify";
import { IMissionDAO } from "../../../dao";
import { IAmqpService, IDecodedMission, IMission, IMissionService } from "../../../definitions";
import { ApiError } from "../../../errors";
import { SHARED_TYPES } from "../../../inversify";

@injectable()
export class MissionService implements IMissionService {
  private readonly amqpService: IAmqpService;
  private readonly missionDAO: IMissionDAO;

  constructor(@inject(SHARED_TYPES.DAO.Mission) missionDAO: IMissionDAO,
              @inject(SHARED_TYPES.Services.Amqp) amqpService: IAmqpService) {
    this.missionDAO = missionDAO;
    this.amqpService = amqpService;
    this.init();
  }

  public async init(): Promise<void> {
    this.amqpService.registerQueue("mission:create", "darkmalt.exchange:mission.create", this.create.bind(this));
    this.amqpService.registerQueue("mission:update", "darkmalt.exchange:mission.update", this.updateOne.bind(this));
    this.amqpService.registerQueue("mission:delete", "darkmalt.exchange:mission.delete", this.deleteOne.bind(this));
  }

  public async create(data: any): Promise<void> {
    const decodedMission = this.missionDAO.parseRabbitMqMessage<IDecodedMission>(data);
    const mission = this.IDecodedMissionToIMission(decodedMission);
    await this.missionDAO.create(mission);
  }

  public async updateOne(data: string): Promise<void> {
    const decodedMission = this.missionDAO.parseRabbitMqMessage<IDecodedMission>(data);
    if (!decodedMission.id) {
      const err = new ApiError(400, "Update/Mission id is required")
      ApiError.saveLog(err);
      throw err;
    };
    const mission = this.IDecodedMissionToIMission(decodedMission);
    await this.missionDAO.update(mission.id, mission);
  }

  public async deleteOne(data: string): Promise<void> {
    const mission = this.missionDAO.parseRabbitMqMessage<IDecodedMission>(data);
    if (!mission.id) {
      const err = new ApiError(400, "Delete/Mission id is required")
      ApiError.saveLog(err);
      throw err;
    };
    await this.missionDAO.delete(mission.id);
  }

  private IDecodedMissionToIMission(decodedMission: IDecodedMission): IMission {
    return {
      id: decodedMission.id,
      description: decodedMission.description,
      title: decodedMission.title,
      skills: decodedMission.skills,
      status: decodedMission.status,
      startDate: decodedMission.startDate,
      endDate: decodedMission.endDate,
      profile: {
        premium_until : decodedMission.idAuthor.profile.premium_until,
        name: decodedMission.idAuthor.profile.name,
      }
    }
  }
}
