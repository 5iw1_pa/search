import { ISearch } from "../interfaces";

export interface ISearchService {
  init(): Promise<void>;
  create(data: ISearch): Promise<void>;
  updateOne(id: string, data: ISearch): Promise<void>;
  deleteOne(id: string): Promise<void>;
}
