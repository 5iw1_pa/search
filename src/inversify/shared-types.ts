export const SHARED_TYPES = {
  Server: Symbol.for('Server'),
  DAO: {
    Search: Symbol.for('SearchDAO'),
    Mission: Symbol.for('MissionDAO'),
    User: Symbol.for('UserDAO'),
  },
  Services: {
    Search: Symbol.for('SearchService'),
    Mission: Symbol.for('MissionService'),
    User: Symbol.for('UserService'),
    Amqp: Symbol.for('AmqpService'),
  },
};
