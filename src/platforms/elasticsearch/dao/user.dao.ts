import {AbstractElasticSearchDAO} from './abstract-es.dao';
import {IUserDAO} from '../../../dao';
import {inject, injectable} from 'inversify';
import {TYPES} from '../inversify';
import { Client } from '@elastic/elasticsearch';
import { IUser } from '../../../definitions';

@injectable()
export class UserDAO extends AbstractElasticSearchDAO<IUser> implements IUserDAO {
  get index(): string {
    return 'user';
  }

  public constructor(@inject(TYPES.Connection) elasticsearch: Client) {
    super(elasticsearch);
  }

  get model(): Client {
    return this.elasticsearch;
  }
}
