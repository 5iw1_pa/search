import { IUser } from "../definitions";
import { DAO } from "./interface.dao";

export interface IUserDAO extends DAO<IUser> {}
