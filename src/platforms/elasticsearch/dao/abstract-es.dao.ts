import { inject, injectable } from "inversify";
import { TYPES } from "../inversify";
import { DAO } from "../../../dao";
import { Client } from "@elastic/elasticsearch";
import { SearchParamsDTO } from "../../../dto";

@injectable()
export abstract class AbstractElasticSearchDAO<C> implements DAO<C> {
  readonly elasticsearch: Client;
  abstract get index(): string;

  protected constructor(@inject(TYPES.Connection) elasticsearch: Client) {
    this.elasticsearch = elasticsearch;
  }
  public async create(data: C): Promise<string> {
    const created = await this.elasticsearch.index({
      index: this.index,
      body: {
        ...data,
      },
    });
    return created.body._id;
  }
  public async update(id: string, data: C): Promise<boolean> {
      const updated = await this.elasticsearch.update_by_query({
        index: this.index,
        body: {
          query: {
            match: {
              id,
            }
          },
          script: {
            source: "ctx._source = params",
            params: data,
            lang: "painless",
          },
        },
        max_docs: 1
      });
      return updated.body.updated == 1;
  }

  public async all<T extends Object>(params: SearchParamsDTO, fields: Array<string>, limit: number = 10000, offset?: number): Promise<C[]> {
    const body = params.s ? 
    {
      query: {
        "multi_match": {
          query: params.s,
          fields: fields,
          type: "phrase_prefix",
          tie_breaker: 0.3
        }
      }
    } :
    undefined;

    const result = await this.elasticsearch.search({
      index: this.index,
      size: limit,
      from: offset,
      body: body,
    });

    return result.body.hits.hits
      .sort(function (hitA: any, hitB: any) {
        // Sort by premium first then by score
        const hitA_premium_until: string|null = hitA._source.profile ? hitA._source.profile.premium_until : null;
        const hitB_premium_until: string|null = hitB._source.profile ? hitB._source.profile.premium_until : null
        if (hitA_premium_until && (new Date(hitA_premium_until).getTime() > (new Date()).getTime())) return -1;
        if (hitB_premium_until && (new Date(hitB_premium_until).getTime() > (new Date()).getTime())) return 1;
        return hitA._score - hitB._score
      })
      .map((hit: any) => hit._source);
  }

  public async findById(id: string): Promise<C | null> {
    const result = await this.elasticsearch.search({
      index: this.index,
      size: 1,
      body: {
        query: {
          match: {
            id,
          }
        }
      }
    });
    return result.body.hits.hits.length ? result.body.hits.hits[0]._source : null;
  }

  public async delete(id: string): Promise<boolean> {
    const deleted = await this.elasticsearch.deleteByQuery({
      index: this.index,
      max_docs: 1,
      body: {
        query: {
          match: {
            id,
          }
        }
      },
    });
    return deleted.body.deleted == 1;
  }

  public parseRabbitMqMessage<C>(data: any): C {
    const buffer = Buffer.from(data.content);
    return JSON.parse(buffer.toString()) as C;
  }
}
