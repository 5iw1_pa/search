import { IMission } from "../interfaces";

export interface IMissionService {
  init(): Promise<void>;
  create(data: IMission): Promise<void>;
  updateOne(id: string, data: IMission): Promise<void>;
  deleteOne(id: string): Promise<void>;
}