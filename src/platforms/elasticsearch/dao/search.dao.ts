import {AbstractElasticSearchDAO} from './abstract-es.dao';
import {ISearchDAO} from '../../../dao';
import {inject, injectable} from 'inversify';
import {TYPES} from '../inversify';
import { Client } from '@elastic/elasticsearch';
import { ISearch } from '../../../definitions';

@injectable()
export class SearchDAO extends AbstractElasticSearchDAO<ISearch> implements ISearchDAO {
  get index(): string {
    return 'test';
  }

  public constructor(@inject(TYPES.Connection) elasticsearch: Client) {
    super(elasticsearch);
  }

  get model(): Client {
    return this.elasticsearch;
  }
}
