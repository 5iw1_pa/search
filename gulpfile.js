const gulp = require('gulp');
const ts = require('gulp-typescript');
const mergeStream = require('merge-stream');
const sourcemaps = require('gulp-sourcemaps');
const del = require('del');

const tsProject = ts.createProject('./tsconfig.json');

gulp.task('clean', () => {
    return del(tsProject.options.outDir);
});

gulp.task('compile', () => {
    let compileStream = tsProject.src();
    if (tsProject.options.sourceMap === true) {
        compileStream = compileStream.pipe(sourcemaps.init());
    }
    compileStream = compileStream.pipe(tsProject());
    let jsStream = compileStream.js;
    if (tsProject.options.sourceMap === true) {
        jsStream = jsStream.pipe(sourcemaps.write())
    }
    return mergeStream(
        compileStream.dts,
        jsStream
    ).pipe(gulp.dest(tsProject.options.outDir));
});

gulp.task('default', gulp.series(
    'clean',
    'compile'
));
